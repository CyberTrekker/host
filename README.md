ALERT: The latest versions of these hosts files contain a social media section of which blocks access to certain social media sites (Facebook et al). If you need to use those sites, then you ostensibly have two options open to you:

- Comment out the pertinent hosts included in the hosts file by placing an # symbol at the beginning of the particular line(s).
- Delete entirely that part of the social media section in the hosts file that's applicable to your requirement.

# Hosts File

**Purpose**

The hosts file is one of the systems facilities of which assists in addressing network nodes in, naturally enough, a computer network. It's a common part of an operating system's (OS) Internet Protocol (IP) implementation. The hosts file serves the function of translating human-friendly host names into numeric protocol addresses (called IP addresses) that identify and locate a host in an IP network.

In some operating systems, the contents of the hosts file is used preferentially to alternative name resolution methods (such as the Domain Name System or DNS), but many systems implement name service switches (for example, nsswitch.conf for Linux and Unix) to provide customization. Unlike remote DNS resolvers, the host file is is under the direct control of the local computer's administrator.

**File Content**

The hosts file contains lines of text consisting of an IP address in the first text field, followed by one or more names. Each field is separated by white space (for historical reasons, tabs are preferred), but spaces are also used. Comment lines can be included and are indicated by a hash sign (#), being in the first position or beginning of the comment line. Blank lines in a hosts file are ignored.

Where the hosts file is located is operating system dependent. You canget some idea of these in the beginning (commented out) section of the hosts file in this repository.

As an example of a basic host file, it may contain the following:

`127.0.0.1 localhost loopback`

`::1       localhost`

The foregoing example only contains entries for the loopback addresses of the system and their host names. This is a typical default content of the hosts file. It illustrates that an IP address may have multiple host names (localhost and loopback), along with a host and may be mapped to both IP4 and IP6 IP addresses. Shown on the first and second lines, respectively.

The second line in this example is on the next line and directly under the first line. There is no line spacing.

**Extended Applications**

The hosts file may be used, in its functioning of resolving host names, to define any hostname or domain name for use in the local system.

One such use is, naturally, redirecting local domains by web service and intranet developers or administrators to to define locally defined domaines in a LAN, for various purposes. This includes accessing a company's internal resources , or to test local websites in development.

Another usage of the hosts file is for Internet resource blocking. In such a use case the hosts file can be used to block online advertising, the domains of known malicious resources and servers containing spyware and adware or other types of malware. This is accomplished by adding entries of such sites to redirect requests to either a non-existing address or to a harmless destination like the local machine. The hosts file can be populated with entries of known undesirable Internet resources in order to facilitate the protection of the machine from these sites.
